# Bespoke stacker game
Playable Tetris-like in Bespoke
![](https://you.have.fail/ed/BespokeSynth_FajTzhpJOa.png)
NOTE: When opening the `stacker.bsk` file, the pulser speed is not correctly restored. Change it to 240, or the game will
run very slowly (ctrl-click to increase upper bound).

# Controls when playing by keyboard
These trigger note events from the `keyboard` module.
You could also set up a MIDI controller to play.

- `w` - hard drop
- `a` - move left
- `s` - move right
- `d` - soft drop

- `y` - rotate 180
- `g` - rotate cw
- `h` - hold
- `j` - rotate ccw

(this is my typical control scheme but with arrow keys relocated to `yghj`)
