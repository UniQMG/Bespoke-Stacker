import sampleplayer
import grid
import random
from time import time as time_

# Requires considerable setup! Open `stacker.bsk` in Bespoke instead.

class Display:
   def __init__(self, game_):
      self.interpolate = True # draws lines at a constant speed (based on length)
      self.resolution = 2 # samples per unit of distance, requires interpolate
      self.overdraw = 1 # number of times to go over the same line
      self.blanking_period = 1/60 # how long to mute the doublebuffer sampler
      self.last_draw = 0
      self.frame = 0
      self.game_ = game_

   def grid(self, w, h, contents, g=None):
      # horizontal lines and block fills
      for iy, row in enumerate(contents):
         for ix, block in enumerate(row):
            yield (ix, iy) # move along line

            if block == "filled" or block == "piece":
               yield (ix+1, iy+1) # |x| in block
               yield (ix, iy)
               yield (ix+1, iy)
               yield (ix, iy+1)
               yield (ix+1, iy)
               if g is not None:
                  g.set(ix, (h-1)-iy, 1)
            if block == "preview":
               yield (ix+1, iy+1) # |/| in block
               yield (ix, iy)

         yield (w, iy) # finish rightmost line
         yield (0, iy) # return to left column

      yield (0, h) # draw bottommost line
      yield (w, h)
      yield (0, h)

      # vertical lines
      yield (0, 0)
      for ix in range(w+1):
         yield (ix, 0)
         yield (ix, h)
         yield (ix, 0)
      yield (0, 0) # return to origin

   def generate_points(self):
      g = grid.get('grid')
      g.set_grid(10, 20)
      g.clear()

      draw = [[block.filled and "filled" or None for block in row] for row in self.game_.matrix]
      for dy in range(20):
         if self.game_.can_move(0, -dy):
            continue
         dy -= 1
         for px, py in self.game_.piece.minos:
            if py - dy < 0 or py - dy > 19: continue
            draw[py - dy][px] = "preview"
         break
      for px, py in self.game_.piece.minos:
         draw[py][px] = "piece"

      # horizontal lines and block fills
      for x,y in self.grid(10, 20, draw, g):
         yield (x, y)


      draw = [[None for _ in range(4)] for _ in range(4)]
      mx = 4
      my = 4
      if self.game_.hold is not None:
         for x,y in self.game_.hold.minos:
            mx = min(x, mx)
            my = min(y, my)
         for x,y in self.game_.hold.minos:
            draw[y-my][x-mx] = "piece"
      for x,y in self.grid(4, 4, draw):
         yield (x*0.75-3, y*0.75+17)

      for b in range(min(len(self.game_.bag), 5)):
         draw = [[None for _ in range(4)] for _ in range(4)]
         mx = 4
         my = 4
         for x,y in self.game_.bag[b].minos:
            mx = min(x, mx)
            my = min(y, my)
         for x,y in self.game_.bag[b].minos:
            draw[y-my][x-mx] = "piece"
         for x,y in self.grid(4, 4, draw):
            yield (x*0.75+10, y*0.75+17-b*3)

   def step_lines(self, axis):
      overdraw = 1
      speed = 10 * overdraw
      last = (0, 0)
      for point in self.generate_points():
         point = (point[0], -point[1])
         if not last:
            last = point
            continue
         (x1, y1) = last
         (x2, y2) = point
         last = point

         dist = math.floor(math.sqrt((x2 - x1)**2 + (y2 - y1)**2)*self.resolution)
         if not self.interpolate: dist = 1
         for i in range(dist):
            if axis == 'x':
               yield (x1 + (x2 - x1) * (i * self.overdraw % dist)/dist) * 1/20 - 0.25
            else:
               yield (y1 + (y2 - y1) * (i * self.overdraw % dist)/dist) * 1/20 + 0.5

   def secs(self):
      measure = bespoke.get_measure_time()
      bpm = bespoke.get_tempo()
      beats = me.get("transport~timesigbottom")
      return measure * bpm/60 * beats

   def can_redraw(self):
      return self.secs() > self.last_draw + 1/60

   def step(self):
      self.frame += 1
      self.last_draw = self.secs()

      blank = self.blanking_period / (bespoke.get_tempo()/60 * me.get("transport~timesigbottom"))
      me.output(blank)

      x = 'x'
      y = 'y'
      b1 = [x for x in self.step_lines('x')]
      b2 = [y for y in self.step_lines('y')]
      smx1 = sampleplayer.get(x)
      smy1 = sampleplayer.get(y)

      me.set("hold~input", 1)
      me.schedule_set(blank*4, "hold~input", 0)
      smx1.fill(b1)
      smy1.fill(b2)
      me.schedule_set(blank, f"x~play", 1)
      me.schedule_set(blank, f"y~play", 1)

      # smx1.play_cue(0)
      # smy1.play_cue(0)

      speed = smx1.get_length_seconds() * 18
      if me.get("a") > 0:
         speed *= me.get("a")
      me.set(f"{x}~speed", speed)
      me.set(f"{y}~speed", speed)

class Block:
   def __init__(self):
      self.filled = False

class Piece:
   def __init__(self, id):
      self.id = id
      self.minos = []
      self.kicks = []
      self.pivot = [0,0]
      self.rotation = 0
      self.reset()

   def reset(self):
      jlstzKicks = []
      jlstzKicks.append({ 'from': 0, 'to': 1, 'tests': [[0, 0], [-1, 0], [-1, 1], [ 0,-2], [-1,-2]] })
      jlstzKicks.append({ 'from': 1, 'to': 0, 'tests': [[0, 0], [ 1, 0], [ 1,-1], [ 0, 2], [ 1, 2]] })
      jlstzKicks.append({ 'from': 1, 'to': 2, 'tests': [[0, 0], [ 1, 0], [ 1,-1], [ 0, 2], [ 1, 2]] })
      jlstzKicks.append({ 'from': 2, 'to': 1, 'tests': [[0, 0], [-1, 0], [-1, 1], [ 0,-2], [-1,-2]] })
      jlstzKicks.append({ 'from': 2, 'to': 3, 'tests': [[0, 0], [ 1, 0], [ 1, 1], [ 0,-2], [ 1,-2]] })
      jlstzKicks.append({ 'from': 3, 'to': 2, 'tests': [[0, 0], [-1, 0], [-1,-1], [ 0, 2], [-1, 2]] })
      jlstzKicks.append({ 'from': 3, 'to': 0, 'tests': [[0, 0], [-1, 0], [-1,-1], [ 0, 2], [-1, 2]] })
      jlstzKicks.append({ 'from': 0, 'to': 3, 'tests': [[0, 0], [ 1, 0], [ 1, 1], [ 0,-2], [ 1,-2]] })

      iKicks = []
      iKicks.append({ 'from': 0, 'to': 1, 'tests': [[0, 0], [-2, 0], [ 1, 0], [-2,-1], [ 1, 2]] })
      iKicks.append({ 'from': 1, 'to': 0, 'tests': [[0, 0], [ 2, 0], [-1, 0], [ 2, 1], [-1,-2]] })
      iKicks.append({ 'from': 1, 'to': 2, 'tests': [[0, 0], [-1, 0], [ 2, 0], [-1, 2], [ 2,-1]] })
      iKicks.append({ 'from': 2, 'to': 1, 'tests': [[0, 0], [ 1, 0], [-2, 0], [ 1,-2], [-2, 1]] })
      iKicks.append({ 'from': 2, 'to': 3, 'tests': [[0, 0], [ 2, 0], [-1, 0], [ 2, 1], [-1,-2]] })
      iKicks.append({ 'from': 3, 'to': 2, 'tests': [[0, 0], [-2, 0], [ 1, 0], [-2,-1], [ 1, 2]] })
      iKicks.append({ 'from': 3, 'to': 0, 'tests': [[0, 0], [ 1, 0], [-2, 0], [ 1,-2], [-2, 1]] })
      iKicks.append({ 'from': 0, 'to': 3, 'tests': [[0, 0], [-1, 0], [ 2, 0], [-1, 2], [ 2,-1]] })

      oKicks = []
      oKicks.append({ 'from': 0, 'to': 1, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 1, 'to': 0, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 1, 'to': 2, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 2, 'to': 1, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 2, 'to': 3, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 3, 'to': 2, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 3, 'to': 0, 'tests': [[0, 0]] })
      oKicks.append({ 'from': 0, 'to': 3, 'tests': [[0, 0]] })

      if self.id == 'I': self.minos, self.kicks = ([[-1, 0], [ 0, 0], [ 1, 0], [ 2, 0]], iKicks)
      if self.id == 'J': self.minos, self.kicks = ([[-1, 1], [-1, 0], [ 0, 0], [ 1, 0]], jlstzKicks)
      if self.id == 'L': self.minos, self.kicks = ([[ 1, 1], [-1, 0], [ 0, 0], [ 1, 0]], jlstzKicks)
      if self.id == 'O': self.minos, self.kicks = ([[ 0, 1], [ 1, 1], [ 0, 0], [ 1, 0]], oKicks)
      if self.id == 'S': self.minos, self.kicks = ([[ 0, 1], [ 1, 1], [-1, 0], [ 0, 0]], jlstzKicks)
      if self.id == 'T': self.minos, self.kicks = ([[ 0, 1], [-1, 0], [ 0, 0], [ 1, 0]], jlstzKicks)
      if self.id == 'Z': self.minos, self.kicks = ([[-1, 1], [ 0, 1], [ 0, 0], [ 1, 0]], jlstzKicks)
      self.pivot = [0,0]

class Game:
   def __init__(self):
      self.matrix = [[Block() for _ in range(10)] for _ in range(20)]

      self.keys = {k: False for k in ["L", "SD", "HD", "R", "CCW", "HOLD", "180", "CW"]}

      self.handling = { 'das': 9, 'arr': 1, 'sdf': 4 } # frames
      self.lock_delay = 30 # frames
      self.lock_resets = 15
      self.gravity = 0.1 # rows per frame

      self.charge = [0, 0] # L / R
      self.lock_reset_counter = 0
      self.lock_timer = 0
      self.gravity_acc = 0

      self.bag = []
      self.piece = None
      self.hold = None

      self.gameover = False
      self.dirty = False
      self.spawn_piece()

   def spawn_piece(self, newpiece=None):
      if newpiece is None:
         while len(self.bag) < 14:
            newbag = ["I", "J", "L", "O", "S", "T", "Z"]
            random.shuffle(newbag)
            for name in newbag:
               self.bag.append(Piece(name))
         newpiece = self.bag.pop(0)
      newpiece.reset()
      self.piece = newpiece
      self.move(5, 18)
      if not self.can_move(0, 0):
         self.gameover = True
         me.set("gamestatus~input", 0)
      self.dirty = True

   def apply_transform(self, mino, dx, dy, rot, kick):
      x, y = mino
      angle = rot * math.pi
      x -= self.piece.pivot[0]
      y -= self.piece.pivot[1]
      for _ in range((rot % 4 + 4) % 4):
         x, y = (y, -x)
      x += self.piece.pivot[0] + dx + kick[0]
      y += self.piece.pivot[1] + dy + kick[1]
      return (x, y)

   def find_kick(self, dx, dy, rot=0):
      target_rotation = ((self.piece.rotation + rot) % 4 + 4) % 4

      kickset = [[0, 0]]
      if rot != 0:
         for ks in self.piece.kicks:
            if ks['from'] == self.piece.rotation and ks['to'] == target_rotation:
               kickset = ks['tests']
               break

      for kick in kickset:
         for mino in self.piece.minos:
            (x, y) = self.apply_transform(mino, dx, dy, rot, kick)
            if self.is_filled(x, y):
               break # continue outer
         else:
            return kick

      return None

   def can_move(self, dx, dy, rot=0):
      return self.find_kick(dx, dy, rot) is not None

   def move(self, dx, dy, rot=0):
      kick = self.find_kick(dx, dy, rot) or [0, 0]

      for mino in self.piece.minos:
         mino[0], mino[1] = self.apply_transform(mino, dx, dy, rot, kick)

      self.piece.rotation = ((self.piece.rotation + rot) % 4 + 4) % 4
      self.piece.pivot[0] += dx + kick[0]
      self.piece.pivot[1] += dy + kick[1]
      self.dirty = True
      if self.lock_timer > 0 and self.lock_reset_counter < self.lock_resets:
         self.lock_timer = 0
         self.lock_reset_counter += 1

   def lock(self):
      for x, y in self.piece.minos:
         self.matrix[y][x].filled = True
      self.clear_lines()
      self.spawn_piece()

   def clear_lines(self):
      def not_full(line):
         for block in line:
            if not block.filled:
               return True
         self.dirty = True
         return False
      self.matrix = list(filter(not_full, self.matrix))
      while len(self.matrix) < 20:
         self.matrix.append([Block() for _ in range(10)])

   def is_filled(self, x, y):
      if x < 0 or y < 0 or x >= 10 or y >= 20:
         return True

      if self.matrix[y][x].filled:
         return True

      return False

   def input(self, key, isDown):
      if self.gameover: return
      self.keys[key] = isDown
      if key == 'L' and isDown and self.can_move(-1, 0): self.move(-1, 0)
      if key == 'R' and isDown and self.can_move( 1, 0): self.move( 1, 0)
      if key == 'CCW' and isDown and self.can_move(0, 0, -1): self.move(0, 0, -1)
      if key == 'CW'  and isDown and self.can_move(0, 0,  1): self.move(0, 0,  1)
      if key == '180' and isDown and self.can_move(0, 0,  2): self.move(0, 0,  2)
      if key == 'HOLD' and isDown:
         lasthold = self.hold
         self.piece.reset()
         self.hold = self.piece
         self.spawn_piece(lasthold)
      if key == 'SD' and isDown:
         while self.can_move(0, -1):
            self.move(0, -1)
      if key == 'HD' and isDown:
         while self.can_move(0, -1):
            self.move(0, -1)
         self.lock()

   def tick(self):
      if self.gameover: return
      gravity = self.gravity * (self.keys['SD'] and self.handling['sdf'] or 1)

      if self.can_move(0, -1):
         self.lock_timer = 0
         self.lock_reset_counter = self.lock_resets
         self.gravity_acc += self.gravity
      else:
         self.lock_timer += 1
         self.gravity_acc = 0

      while self.gravity_acc > 1:
         if self.can_move(0, -1):
            self.move(0, -1)
         self.gravity_acc -= 1

      if self.lock_timer > self.lock_delay:
         self.lock()

      for key, charge, dir in (('L', 0, -1), ('R', 1, 1)):
         just_triggered = self.charge[charge] < self.handling['das']
         if self.keys[key]:
            self.charge[charge] += 1
         else:
            self.charge[charge] = 0

         while True:
            overcharge = self.charge[charge] - self.handling['das']
            if overcharge > 0 and just_triggered:
               just_triggered = False
               if not self.can_move(dir, 0): break
               self.move(dir, 0)
            elif overcharge > self.handling['arr']:
               self.charge[charge] -= self.handling['arr']
               if not self.can_move(dir, 0): break
               self.move(dir, 0)
            else:
               break

display = Display(Game())
last = time_()
dtavg = 0

me.set("gamestatus~input", 1)

def on_pulse():
   global last
   global dtavg
   dt = time_() - last
   dtavg += (dt - dtavg) * 0.1
   me.output(f"{round(1/(dtavg or 1), 1)}hz")
   last += dt
   display.game_.tick()
   if display.game_.dirty and display.can_redraw():
      display.game_.dirty = False
      display.step()

inputs = ["L", "HD", "SD", None, "R", None, None, "CCW", "180", "HOLD", None, "CW"]
def on_note(pitch, vel):
   key = inputs[pitch % 12]
   if key is None: return
   display.game_.input(key, vel > 0)
